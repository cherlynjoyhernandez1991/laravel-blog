<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// Require user authentication to use
use Illuminate\Support\Facades\Auth;
use App\Models\Post;
use App\Models\PostLike;
use App\Models\PostComment;

class PostController extends Controller
{
    public function create() {
        return view('posts.create');
    }

    // get request 
    public function store(Request $request) {
        // if user is auth
        if (Auth::user()) {

            // create a new Post object from the Post model
            $post = new Post;

            //define the properties of the $post object using received form data
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            // save auth user id
            $post->user_id = (Auth::user()->id);
            // save the post object to database
            $post->save();

            return redirect('/posts');
        } else {
            return redirect('/login');
        }
    }

    public function index() {
        // get all posts from the database
        $posts = Post::where('isActive', true)->get();

        return view('posts.index')->with('posts', $posts);
    }

    public function home() {
        // get all posts from the database
        $posts = Post::inRandomOrder()->limit(3)->get();
        return view('welcome')->with('posts', $posts);
    }

    public function myPosts() {
        // if the user is logged in
        if (Auth::user()) {
            $posts = Auth::user()->posts; // retrieve the user's own posts

            return view('posts.index')->with('posts', $posts);
        } else {
            return redirect('/login');
        }
    }

    public function show($id) {
        $post = Post::find($id);

        return view('posts.show')->with('post', $post);
    }

    public function edit($id) {
        $post = Post::find($id);
        return view('posts.edit')->with('post', $post);

        
    }

    public function update(Request $request, $id) {
        $post = Post::find($id);

        if (Auth::user()->id == $post->user_id) {
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->update();
        }

        return view('posts.show')->with('post', $post);
    }

    public function destroy($id) {
        $post = Post::find($id);

        if (Auth::user()->id == $post->user_id) {
            $post->delete();
        }

        return redirect('/posts');
    }

    public function archive($id) {
        $post = Post::find($id);

        if (Auth::user()->id == $post->user_id) {
            $post->isActive = false;
            $post->save();
        }

        return redirect('/posts');
    }

    public function like($id) {
        $post = Post::find($id);
        $user_id = Auth::user()->id;

        // check if the authenticated user is NOT the post author
        if ($user_id != $post->user_id) {
            // check post already been like by the user
            if ($post->likes->contains('user_id', $user_id)) {
                // dekete the like made by the user to unlike the post
                PostLike::where('post_id', $post->id)->where('user_id', $user_id)->delete();
            } else {
                $postLike = new PostLike;

                $postLike->post_id = $post->id;
                $postLike->user_id = $user_id;

                $postLike->save();
            }
            return redirect("/posts/$id");
        }
    }

    public function comment(Request $request, $id) {
        $post = Post::find($id);
        $user_id = Auth::user()->id;

        if (Auth::user()) {
            $postComment = new PostComment;

            $postComment->post_id = $post->id;
            $postComment->user_id = $user_id;
            $postComment->content = $request->input('comment');

            $postComment->save();
        }

        return redirect("/posts/$id");
    }


}
