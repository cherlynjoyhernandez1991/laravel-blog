@extends('layouts.app')

@section('content')

<div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
    <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">

        <div class="posts-list">
            <div class="card text-center">
                <div class="card-body">
                    <h3>{{$post->title}}</h3>
                    <h6>Author: {{$post->user->name}}</h6>
                    <h6>Created at: {{$post->created_at}}</h6>
                    <p>{{$post->content}}</p>
                </div>

                @if (Auth::id() != $post->user_id)
                    <form class="d-line mb-3" method="POST" action="/posts/{{$post->id}}/like">
                        @method('PUT')
                        @csrf
                        @if($post->likes->contains('user_id', Auth::id()))
                            <button type="submit" class="btn btn-danger">Unlike</button>
                        @else
                            <button type="submit" class="btn btn-success">Like</button>
                        @endif
                    </form>
                @endif

                <div class="comment-section">
                    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modalComment"><i class="bi bi-chat-left"></i> Comment</button>

                    <div class="mt-3">

                        @foreach($post->comment as $details)
                            @if($details->post_id == $post->id)
                                <h6>{{$details->user->name}}</h6>
                                <p>{{$details->content}}</p>
                            @endif
                        @endforeach
                    </div>
                </div>

                {{-- Modal --}}
                <div class="modal fade" id="modalComment" tabindex="-1" role="dialog" aria-labelledby="modalComment" aria-hidden="true">
                    <div  class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="modalLongTitle">New Comment</h5>
                                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            <form method="POST" action="/posts/{{$post->id}}/comment">
                                @method('PUT')
                                @csrf
                                <div class="modal-body">
                                    <textarea class="form-control" id="comment" name="comment" rows="3"></textarea>
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Save Comment</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                {{-- Modal --}}

                <div class="mt-3">
                    <a href="/posts" class="card-link">View all posts</a>
                </div>
            </div>
        </div>
    </div>
</div>
 
@endsection