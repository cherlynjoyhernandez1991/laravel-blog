<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// route that will return a view containing all posts
Route::get('/', [PostController::class, 'home']);

// route to return save or create new data
Route::get('/posts/create', [PostController::class, 'create']);

// route to return view all posts
Route::post('/posts', [PostController::class, 'store']);

// route that will return a view containing all posts
Route::get('/posts', [PostController::class, 'index']);

// route that will return a view containing only the authenticated user's posts
Route::get('/myposts', [PostController::class, 'myposts']);

// route that will show a specific posts view based on the URL parameter's ID
Route::get('/posts/{id}', [PostController::class, 'show']);

// route that will shpw a specific posts on edit page based on the URL parameter's ID
Route::get('/posts/{id}/edit', [PostController::class, 'edit']);

// route that will update a specific post by user
Route::put('/posts/{id}', [PostController::class, 'update']);

// route that will delete a specific post by user
Route::delete('/posts/{id}', [PostController::class, 'archive']);

// route that will enable users to like/unlike posts
Route::put('/posts/{id}/like', [PostController::class, 'like']);

// route that will enable users to comment in posts
Route::put('/posts/{id}/comment', [PostController::class, 'comment']);

Auth::routes();

Route::get('/home', [HomeController::class, 'index']);
